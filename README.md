# private-bitbucket-buildpack

Used for Heroku deployments where private SDKs and libraries are pulled via NPM.

Required environment variables (config settings)

* `PRIVATE_DEPLOY_KEY` - base64 encoded private deploy key
* `PRIVATE_DEPLOY_HOST` - default `bitbucket.org`. Change for VPN based deployments or other hosts.

Set this buildpack in front of any others that will require private dependencies.

```
heroku buildpacks:add -i 1 https://bitbucket.org/reason/private-bitbucket-buildpack.git
```
